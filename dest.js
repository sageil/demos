
// import Logger from './shared';

const numbers = [1, 2, 3, 5, 8, 9];
// const [a, b, ...c] = numbers;
for (let i = 0; i < 10; i += 1) {
  console.log(i);
}
/* ?. */
// Logger('A', a);
// Logger('B', b);
// Logger('C', c);
// Logger('Hello');
const ab = numbers.includes(1, 0);
console.log(ab);

class Person {
  constructor(name) {
    this.name = name;
  }

  greet() {
    console.log(`my name is ${this.name}`);
  }
}
class Sammy extends Person {
  constructor() {
    super('sammy');
  }
}

const sammy /* ? */ = new Sammy('sammy'); // ?
const c = Object.getPrototypeOf(sammy); // ?
// console.log(Object.getPrototypeOf(sammy)); /* / */
