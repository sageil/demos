const { Logger, people, Fib } = require('./shared');


// finds all items matching the criteria
const developers = people.filter(d => (d.profession === 'Developer'));
Logger('developers', developers);

// stops after finding the first item
const males = people.find(f => f.gender === 'male');
Logger('males', males);

// filter and return an array of objects
const filteredObjects = people.filter(f => f.gender === 'male').map(p => ({

  profession: p.profession,
  fName: p.fName,
  lName: p.lName,
  gender: p.gender,

}));

Logger('FilteredObjects', filteredObjects);
// chain filter and map to filter and transform data.
const femaleUpperCaseGender = people.filter(f => f.gender === 'female').map(p => ({
  gender: p.gender.toLocaleUpperCase(), profession: p.profession, fName: p.fName, lName: p.lName,
}));

Logger('FemaleUpperCaseGender', femaleUpperCaseGender);

// Sort by gender
const sorted = people.sort((a, b) => (a.gender > b.gender ? -1 : 1));

Logger('Sorted', sorted);
// create an object with two arrays, one for MALES and the other for FEMALES
const split = people.reduce((genderSplits, val) => {
  const gender = genderSplits;
  const Pluralgender = `${val.gender.toLocaleUpperCase()}S`;

  gender[Pluralgender] = gender[Pluralgender] || [];

  gender[Pluralgender].push({
    fName: val.fName,
    lName: val.lName,
    profession: val.profession,
    gender: val.gender,
  });

  return gender;
}, {});
const mapped = people.map(x => x.gender.toLocaleLowerCase());
Logger('Mapped', mapped);


Logger('Split', split);
for (let i = 0; i < 10; i += 1) {
  // eslint-disable-next-line no-console
  console.log(Fib(i));
}

Logger('Done', null);
