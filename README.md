# Functional Javascript & Pure functions

To download and run code locally using [VsCode](https://code.visualstudio.com/ "VsCode"):

1- git clone [https://bitbucket.org/sageil/demos.git](https://bitbucket.org/sageil/demos.git)

2- run npm -i on the directory

3- Open the terminal using Ctrl + "~"

4- Execute the code using node by typing `node index.js`




