
const Logger = (message, object) => {
  // eslint-disable-next-line no-console
  console.log('\r\n');
  // eslint-disable-next-line no-console
  console.log(message.toLocaleUpperCase());

  if (object !== null) {
    // eslint-disable-next-line no-console
    console.log(JSON.stringify(object, null, 4));
    // eslint-disable-next-line no-console
    console.log('\r\n');
  }

  // eslint-disable-next-line no-console
  console.log('===============================================================');
};

const people = [
  {
    fName: 'John', lName: 'Doe', gender: 'male', profession: 'Developer',
  },
  {
    fName: 'Jasmine', lName: 'Robinson', gender: 'female', profession: 'Painter',
  },
  {
    fName: 'Michael', lName: 'Jackson', gender: 'male', profession: 'Entertainer',
  },
  {
    fName: 'John', lName: 'Johnson', gender: 'male', profession: 'Developer',
  },
  {
    fName: 'Jeff', lName: 'Ryerson', gender: 'male', profession: 'Technician',
  },
  {
    fName: 'Alex', lName: 'Johansen', gender: 'male', profession: 'Teacher',
  },
  {
    fName: 'Joanna', lName: 'Jamieson', gender: 'female', profession: 'Developer',
  },
  {
    fName: 'Cortney', lName: 'Davidson', gender: 'female', profession: 'Entertainer',
  }];

function Fib(n) {
  return (n < 2) ? n : (Fib(n - 1) + Fib(n - 2));
}

module.exports.Logger = Logger;
module.exports.people = people;
module.exports.Fib = Fib;
